import Layout from './Layout';
import {withRouter} from 'next/router';

const Post = withRouter((props) => {
	return <Layout>
		<h3>Post Details Page :: {props.router.query.title}</h3>
	</Layout>
});
export default Post